<?php
/**
 * @file
 * ctools plugin definitions.
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function search_replace_ctools_plugin_api() {
  return array('version' => 1);
}

/**
 * Implements hook_ctools_plugin_type
 */
function search_replace_ctools_plugin_type() {
  return array(
    'source_plugin' => array(),
    'search_plugin' => array(),
    'style_plugin' => array(),
  );
}

/**
 * Register our modules with ctools
 *
 * @param $module
 * @param $plugin
 * @return
 *   Folder for our plugins.
 */
function search_replace_ctools_plugin_directory($module, $plugin) {
  if ($module == 'search_replace' && $plugin === 'source_plugin') {
    return "plugins/$plugin";
  }
  else if ($module == 'search_replace' && $plugin === 'search_plugin') {
    return "plugins/$plugin";
  }
  else if ($module == 'search_replace' && $plugin === 'style_plugin') {
    return "plugins/$plugin";
  }
}