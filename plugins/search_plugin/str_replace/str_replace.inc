<?php
/**
* @file Setting up a plugin for search_replace input filter.
* The fields 'title', 'plugin_name' and 'callback_function' are required.
* 'title' is the displayname in the search_replace settingsform selectbox.
* 'plugin_name' must be a unique machine readable name.
* 'description' simple infotext.
* 'callback_function' is the function that will call
* from search_replace module to get data from this plugin.
* 'settings_form' can be used to provide a settings form that will display
* under the vertical tab for search_ and replace input filter.

* @var array
*/
$plugin = array(
  'title' => t('str replace plugin'),
  'plugin_name' => 'str_replace',
  'description' => t('Use str_replace to find matches.'),
  'callback_function' => 'str_replace_plugin_callback',
  'settings_form' => 'str_replace_plugin_settings_form',
);

/**
 * Return the FAPI Form Array for this plugin.
 * @param $settings
 * return array
 */
function str_replace_plugin_settings_form($settings) {
  $form['str_replace_plugin_field_source_row'] = array(
    '#type' => 'textfield',
    '#title' => t('enter csv file path'),
    '#default_value' => isset($settings['settings']['str_replace_plugin_field_source_row']) ? $settings['settings']['str_replace_plugin_field_source_row'] : 0,
    '#description' => t('Enter the name of the row from the source plugin for this fiel.'),
    '#size' => 20,
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="filters[search_replace][settings][search_replace_filter_plugin_search]"]' => array(
          'value' => $settings['plugin']['plugin_name'],
        ),
      ),
    ),
  );
  return $form;
}

function str_replace_plugin_callback($settings) {
  $search = $settings['source'][$settings['settings']['str_replace_plugin_field_source_row']];
  return str_replace($search, $settings['replace'], $settings['text']);
}
