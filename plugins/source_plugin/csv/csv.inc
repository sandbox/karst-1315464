<?php
/**
 * @file Setting up a plugin for search_replace input filter.
 * The fields 'title', 'plugin_name' and 'callback_function' are required.
 * 'title' is the displayname in the search_replace settingsform selectbox.
 * 'plugin_name' must be a unique machine readable name.
 * 'description' simple infotext.
 * 'callback_function' is the function that will call
 * from search_replace module to get data from this plugin.
 * 'settings_form' can be used to provide a settings form that will display
 * under the vertical tab for search_ and replace input filter.

 * @var array
 */
$plugin = array(
    'title' => t('CSV Source Filter'),
    'plugin_name' => 'csv',
    'description' => t('Use a csv file as source.'),
    'callback_function' => 'csv_import_plugin_callback',
    'settings_form' => 'csv_import_plugin_settings_form',
  );

/**
 * Return the FAPI Form Array for this plugin.
 * @param $settings
 * return array
 */
function csv_import_plugin_settings_form($settings) {
  $form['csv_plugin_field_file'] = array(
      '#type' => 'textfield',
      '#title' => t('enter csv file path'),
      '#default_value' => isset($settings['settings']['csv_plugin_field_file']) ? $settings['settings']['csv_plugin_field_file'] : 0,
      '#description' => t('Enter the file path to the csv file to import.'),
      '#size' => 20,
      '#required' => TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="filters[search_replace][settings][search_replace_filter_plugin_source]"]' => array(
            'value' => $settings['plugin']['plugin_name'],
          ),
        ),
      ),
    );
  return $form;
}

function csv_import_plugin_callback($settings) {
  $values = array(
    array(
      'hallo',
      'google',
      'http://www.google.de'
    ),
    array(
          'test',
          'gfdgfdgd',
          'http://www.google.de'
    ),
  );
  $temp = array();
  if($settings['offset'] >= count($values)) {
    return NULL;
  }
  else {
    return array_slice($values, $settings['offset'], $settings['count']);
  }
}
