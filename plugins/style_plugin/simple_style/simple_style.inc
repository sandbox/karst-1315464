<?php
/**
 * @file Setting up a plugin for search_replace input filter.
 * The fields 'title', 'plugin_name' and 'callback_function' are required.
 * 'title' is the displayname in the search_replace settingsform selectbox.
 * 'plugin_name' must be a unique machine readable name.
 * 'description' simple infotext.
 * 'callback_function' is the function that will call
 * from search_replace module to get data from this plugin.
 * 'settings_form' can be used to provide a settings form that will display
 * under the vertical tab for search_ and replace input filter.

 * @var array
 */
$plugin = array(
    'title' => t('Simple Style'),
    'plugin_name' => 'simple_style_plugin',
    'description' => t('Simple Style Plugin.'),
    'callback_function' => 'simple_style_plugin_callback',
    'settings_form' => 'simple_style_plugin_settings_form',
  );

/**
 * Return the FAPI Form Array for this plugin.
 * @param $settings
 * return array
 */
function simple_style_plugin_settings_form($settings) {
  $form['style_plugin_field_title'] = array(
      '#type' => 'textfield',
      '#title' => t('enter title row'),
      '#default_value' => isset($settings['settings']['style_plugin_field_title']) ? $settings['settings']['style_plugin_field_title'] : 0,
      '#description' => t('Enter the name of the row from the source plugin for this fiel.'),
      '#size' => 20,
      '#required' => TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="filters[search_replace][settings][search_replace_filter_plugin_style]"]' => array(
            'value' => $settings['plugin']['plugin_name'],
          ),
        ),
      ),
    );
  $form['style_plugin_field_url'] = array(
      '#type' => 'textfield',
      '#title' => t('enter url row'),
      '#default_value' => isset($settings['settings']['style_plugin_field_url']) ? $settings['settings']['style_plugin_field_url'] : 0,
      '#description' => t('Enter the name of the row from the source plugin for this fiel.'),
      '#size' => 20,
      '#required' => TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="filters[search_replace][settings][search_replace_filter_plugin_style]"]' => array(
            'value' => $settings['plugin']['plugin_name'],
          ),
        ),
      ),
    );
  return $form;
}

/**
 * 
 */
function simple_style_plugin_callback($settings) {
  $title_row = $settings['settings']['style_plugin_field_title'];
  $url_row = $settings['settings']['style_plugin_field_url'];
  return l($settings['source'][$title_row], $settings['source'][$url_row]);
}
